const { resolve } = require("path")

const sassRule = {
  test: /\.scss$/,
  use: [
    { loader: "style-loader" },
    { loader: "css-loader" },
    {
      loader: "sass-loader",
      options: {
        data: '@import "./index.scss";',
        includePaths: [__dirname, "src"]
      }
    }
  ]
}

const typescriptRule = {
  test: /\.tsx?$/,
  use: [
    {
      loader: "ts-loader",
      options: {
        compilerOptions: {
          module: "esnext",
          allowSyntheticDefaultImports: true
        },
        transpileOnly: true,
        allowTsInNodeModules: true
      }
    }
  ]
}

module.exports = {
  mode: "development",
  entry: "./src/index.tsx",
  output: {
    filename: "js/bundle.js",
    path: resolve(__dirname, "public")
  },

  module: {
    rules: [typescriptRule, sassRule]
  },

  resolve: {
    extensions: [".js", ".ts", ".tsx", ".scss"],
    mainFields: ["jsnext:main", "module", "main"]
  },

  devServer: {
    contentBase: resolve(__dirname, "public"),
    historyApiFallback: true,
    port: 9000
  }
}
