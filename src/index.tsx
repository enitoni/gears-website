import React from "react"
import ReactDOM from "react-dom"
import { App } from "./modules/core/components"

// Render app
ReactDOM.render(<App />, document.getElementById("App"))
