import { computed } from "mobx"
import { observer } from "mobx-react"
import React from "react"
import UrlPattern from "url-pattern"
import { historyStore } from "../stores"

export interface Route {
  match: string
  component: React.ReactType
}

export interface RouteComponentProps {
  params: Record<string, string>
}

export interface RouterProps {
  routes: Route[]
}

@observer
export class Router extends React.Component<RouterProps> {
  @computed
  private get match(): [Route, any] | undefined {
    const { routes } = this.props

    for (const route of routes) {
      const pattern = new UrlPattern(route.match)
      const match = pattern.match(historyStore.location.pathname)

      if (match) return [route, match]
    }
  }

  public render() {
    if (!this.match) return null
    const [route, params] = this.match

    const Component = route.component
    return <Component key={route.match} params={params} />
  }
}
