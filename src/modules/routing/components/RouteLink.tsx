import getClassName from "classnames"
import { bind } from "decko"
import { computed } from "mobx"
import { observer } from "mobx-react"
import React from "react"
import UrlPattern from "url-pattern"
import { historyStore } from "../stores"

export interface RouteLinkProps {
  to: string
  match?: string
}

export type PartialAnchorProps = React.AnchorHTMLAttributes<HTMLAnchorElement>

@observer
export class RouteLink extends React.Component<
  RouteLinkProps & PartialAnchorProps
> {
  @bind
  private handleClick(event: React.MouseEvent<HTMLAnchorElement>) {
    const { onClick, to } = this.props

    historyStore.push(to)
    event.preventDefault()

    onClick && onClick(event)
  }

  @computed
  get active() {
    const { match, to } = this.props

    const safeMatch = match || to
    const [safePattern, hash] = safeMatch.split("#")

    const pattern = new UrlPattern(safePattern)

    const locationHash = historyStore.location.hash.replace("#", "")
    const hashMatches = !hash || hash === locationHash

    return pattern.match(historyStore.location.pathname) && hashMatches
  }

  public render() {
    const { to, match, onClick, children, className, ...props } = this.props

    const fullClassName = getClassName(className, {
      "-active": this.active
    })

    return (
      <a
        className={fullClassName}
        href={to}
        onClick={this.handleClick}
        {...props}
      >
        {children}
      </a>
    )
  }
}
