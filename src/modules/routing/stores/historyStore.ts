import { createBrowserHistory } from "history"
import { observable } from "mobx"

const history = createBrowserHistory()

class HistoryStore {
  @observable public location = history.location

  constructor() {
    history.listen(location => (this.location = location))
  }

  public push(path: string, state?: any) {
    history.push(path, state)
  }
}

export const historyStore = new HistoryStore()
