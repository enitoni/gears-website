import React from "react"
import { RouteComponentProps } from "../../routing/components"
import { descriptorStore } from "../stores"
import { ClassDescriptorRenderer } from "./ClassDescriptorRenderer"
import { InterfaceDescriptorRenderer } from "./InterfaceDescriptorRenderer"

const ComponentMap: Record<string, React.ReactType> = {
  class: ClassDescriptorRenderer,
  interface: InterfaceDescriptorRenderer
}

export class DescriptorView extends React.Component<RouteComponentProps> {
  public render() {
    const { kind, name } = this.props.params

    const descriptor = descriptorStore.get(name, kind)
    if (!descriptor) return null
    const Component = ComponentMap[descriptor.kind]
    if (!Component) return null

    return (
      <div className="DescriptorView">
        <Component descriptor={descriptor} />
      </div>
    )
  }
}
