import React from "react"
import { Document } from "../../docs/components"
import { InterfaceDescriptor, InterfacePropertyDescriptor } from "../types"
import { DescriptorHeader } from "./DescriptorHeader"
import { TypeLink } from "./TypeLink"

export interface InterfaceDescriptorRendererProps {
  descriptor: InterfaceDescriptor
}

const renderProperty = (prop: InterfacePropertyDescriptor) => {
  const { name, type, optional } = prop

  return (
    <tr key={name}>
      <td>{name}</td>
      <td>
        <TypeLink type={type} />
      </td>
      <td>{optional ? "Yes" : "No"}</td>
    </tr>
  )
}

export const InterfaceDescriptorRenderer = (
  props: InterfaceDescriptorRendererProps
) => {
  const { descriptor } = props
  const renderedProperties = descriptor.properties.map(renderProperty)

  return (
    <Document>
      <DescriptorHeader descriptor={descriptor} />
      <table>
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Optional</th>
          </tr>
        </thead>
        <tbody>{renderedProperties}</tbody>
      </table>
    </Document>
  )
}
