import React from "react"
import { Document } from "../../docs/components"
import { ClassDescriptor } from "../types"
import { ConstructorRenderer } from "./ConstructorRenderer"
import { DescriptorHeader } from "./DescriptorHeader"

export interface ClassDescriptorRendererProps {
  descriptor: ClassDescriptor
}

export const ClassDescriptorRenderer = (
  props: ClassDescriptorRendererProps
) => {
  const { descriptor } = props

  return (
    <Document>
      <DescriptorHeader descriptor={descriptor} />
      <h2>Constructor</h2>
      <ConstructorRenderer descriptor={descriptor} />
    </Document>
  )
}
