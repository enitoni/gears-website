import React from "react"
import { stringifyType } from "../helpers/stringifyType"
import { ClassDescriptor, FunctionArgument } from "../types"
import { TypeLink } from "./TypeLink"

export interface ConstructorRendererProps {
  descriptor: ClassDescriptor
}

const renderArgument = (arg: FunctionArgument) => {
  const { name, optional, type } = arg

  return (
    <tr key={name}>
      <td>{name}</td>
      <td>
        <TypeLink type={type} />
      </td>
      <td>{optional ? "Yes" : "No"}</td>
    </tr>
  )
}

export const ConstructorRenderer = (props: ConstructorRendererProps) => {
  const { descriptor } = props
  const { name, constructorArgs } = descriptor

  const args = constructorArgs.map(arg => {
    const { name, optional, type } = arg
    return `${name}${optional ? "?" : ""}: ${stringifyType(type)}`
  })

  const code = `const ${name.toLowerCase()} = new ${name}(${args.join(", ")})`

  const renderedArguments =
    constructorArgs.length > 0 ? (
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Optional</th>
          </tr>
        </thead>
        <tbody>{constructorArgs.map(renderArgument)}</tbody>
      </table>
    ) : null

  return (
    <>
      <code>
        <pre>{code}</pre>
      </code>
      {renderedArguments}
    </>
  )
}
