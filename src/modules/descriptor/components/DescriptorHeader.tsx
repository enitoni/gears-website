import React from "react"
import { DocumentTitle } from "../../docs/components"
import { DescriptorLike } from "../types"

export interface DescriptorHeaderProps {
  descriptor: DescriptorLike
}

export const DescriptorHeader = (props: DescriptorHeaderProps) => {
  const { descriptor } = props
  const description = descriptor.description && <p>{descriptor.description}</p>

  return (
    <>
      <DocumentTitle title={descriptor.name} />
      {description}
    </>
  )
}
