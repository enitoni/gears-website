import React from "react"
import { RouteLink } from "../../routing/components"
import { getDescriptorPath } from "../helpers"
import { descriptorStore } from "../stores"
import { Type } from "../types"

export interface TypeLinkProps {
  type: Type
}

export const TypeLink = (props: TypeLinkProps) => {
  const { type } = props
  const { ref, params = [] } = type

  const renderedParamList = params.map((param, i) => {
    const isAtEnd = params.length === i + 1

    return (
      <React.Fragment key={i}>
        <TypeLink type={param} />
        {!isAtEnd && ", "}
      </React.Fragment>
    )
  })

  const renderedParams =
    renderedParamList.length > 0 ? (
      <>
        {"<"}
        {renderedParamList}
        {">"}
      </>
    ) : null

  if (typeof ref === "string") {
    return (
      <span>
        {ref}
        {renderedParams}
      </span>
    )
  }

  if (Array.isArray(ref)) return null

  if (ref.type === "url") {
    return (
      <a target="_blank" href={ref.url}>
        {ref.name}
        {renderedParams}
      </a>
    )
  }

  const descriptor = descriptorStore.get(ref.name, ref.kind)
  if (!descriptor) return <span>INVALID REFERENCE</span>

  const path = getDescriptorPath(descriptor)

  return (
    <RouteLink to={path}>
      {ref.name}
      {renderedParamList}
    </RouteLink>
  )
}
