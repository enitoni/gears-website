import { Descriptor } from "./Descriptor"
import { PropertyDescriptor } from "./PropertyDescriptor"
import { Type } from "./Type"

export interface InterfacePropertyDescriptor
  extends PropertyDescriptor<"interface-property"> {
  optional?: boolean
}

export interface InterfaceDescriptor extends Descriptor<"interface"> {
  properties: InterfacePropertyDescriptor[]
  extends?: Type
}
