import { Descriptor } from "./Descriptor"
import { Type } from "./Type"

export interface PropertyDescriptor<T extends string> extends Descriptor<T> {
  type: Type
}
