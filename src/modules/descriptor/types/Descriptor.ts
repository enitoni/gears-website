export interface Descriptor<T extends string> {
  name: string
  category?: string
  description?: string
  kind: T
}
