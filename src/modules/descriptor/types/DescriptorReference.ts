import { DescriptorLike } from "./DescriptorLike"

export interface DescriptorReference<T extends DescriptorLike["kind"]> {
  type: "reference"
  kind: T
  name: string
  array?: boolean
}
