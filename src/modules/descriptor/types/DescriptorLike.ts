import {
  ClassDescriptor,
  ClassPropertyDescriptor,
  MethodDescriptor
} from "./ClassDescriptor"
import { EventDescriptor } from "./EventDescriptor"
import {
  InterfaceDescriptor,
  InterfacePropertyDescriptor
} from "./InterfaceDescriptor"
import { TypeParamDescriptor } from "./TypeParamDescriptor"

export type DescriptorLike =
  | ClassDescriptor
  | EventDescriptor
  | MethodDescriptor
  | ClassPropertyDescriptor
  | InterfaceDescriptor
  | InterfacePropertyDescriptor
  | TypeParamDescriptor
