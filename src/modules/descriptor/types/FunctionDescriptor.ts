import { Descriptor } from "./Descriptor"
import { Type } from "./Type"
import { TypeParamDescriptor } from "./TypeParamDescriptor"

export interface FunctionArgument {
  name: string
  type: Type
  optional?: boolean
  default?: any
  rest?: boolean
}

export interface FunctionDescriptor<T extends string> extends Descriptor<T> {
  params?: TypeParamDescriptor[]
  arguments?: FunctionArgument[]
  returns?: Type
  async?: boolean
}
