import { Descriptor } from "./Descriptor"
import { Type } from "./Type"

export interface EventDescriptor extends Descriptor<"event"> {
  event: string
  value: Type
}
