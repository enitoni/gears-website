import { DescriptorReference } from "./DescriptorReference"

export interface TypeURL {
  type: "url"
  name: string
  url: string
}

type TypeLike =
  | (string | DescriptorReference<any> | TypeURL)
  | (string | DescriptorReference<any> | TypeURL)[]

export interface Type {
  ref: TypeLike
  params?: Type[]
}
