import { Descriptor } from "./Descriptor"
import { Type } from "./Type"

export interface TypeParamDescriptor extends Descriptor<"type-param"> {
  extends?: Type
  default?: Type
}
