import { Descriptor } from "./Descriptor"
import { DescriptorReference } from "./DescriptorReference"
import { FunctionArgument, FunctionDescriptor } from "./FunctionDescriptor"
import { PropertyDescriptor } from "./PropertyDescriptor"
import { Type } from "./Type"
import { TypeParamDescriptor } from "./TypeParamDescriptor"

export type Access = "private" | "protected" | "public"

export interface MethodDescriptor extends FunctionDescriptor<"method"> {
  access?: Access
  abstract?: boolean
}

export interface ClassPropertyDescriptor
  extends PropertyDescriptor<"class-property"> {
  access?: Access
}

export interface ClassDescriptor extends Descriptor<"class"> {
  params?: TypeParamDescriptor[]
  constructorArgs: FunctionArgument[]
  methods?: MethodDescriptor[]
  properties?: ClassPropertyDescriptor[]
  events?: DescriptorReference<"event">[]
  abstract?: boolean
  extends?: Type
}
