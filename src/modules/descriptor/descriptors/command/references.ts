import { DescriptorReference } from "../../types"

export const CommandLike: DescriptorReference<"class"> = {
  type: "reference",
  name: "CommandLike",
  kind: "class"
}

export const CommandMetadata: DescriptorReference<"interface"> = {
  type: "reference",
  name: "CommandMetadata",
  kind: "interface"
}
