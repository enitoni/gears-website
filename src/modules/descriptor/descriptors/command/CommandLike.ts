import { createReference } from "../../helpers"
import { ClassDescriptor, InterfaceDescriptor } from "../../types"
import { MDN_URLS } from "../urls"
import { CATEGORY } from "./constants"

export const CommandMetadata: InterfaceDescriptor = {
  name: "CommandMetadata",
  description: "Metadata that describes a CommandLike",
  kind: "interface",
  category: CATEGORY,
  properties: []
}

export const CommandLikeOptions: InterfaceDescriptor = {
  name: "CommandLikeOptions",
  description: "Options passed to a CommandLike",
  kind: "interface",
  category: CATEGORY,
  properties: [
    {
      kind: "interface-property",
      name: "metadata",
      type: {
        ref: createReference(CommandMetadata)
      },
      optional: true
    },
    {
      kind: "interface-property",
      name: "resolvers",
      type: {
        ref: MDN_URLS.ARRAY,
        params: [{ ref: "PermissionResolver" }]
      },
      optional: true
    }
  ]
}

export const CommandLike: ClassDescriptor = {
  name: "CommandLike",
  description: "Represents either a Command or CommandGroup",
  kind: "class",
  category: CATEGORY,
  abstract: true,
  constructorArgs: [
    {
      name: "options",
      type: {
        ref: createReference(CommandLikeOptions)
      }
    }
  ]
}
