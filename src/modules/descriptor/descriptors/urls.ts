import { TypeURL } from "../types/Type"

type DiscordUrlKey = "MESSAGE" | "CLIENT_OPTIONS"

export const DISCORDJS_URLS: Record<DiscordUrlKey, TypeURL> = {
  MESSAGE: {
    type: "url",
    name: "Message",
    url: "https://discord.js.org/#/docs/main/stable/class/Message"
  },
  CLIENT_OPTIONS: {
    type: "url",
    name: "ClientOptions",
    url: "https://discord.js.org/#/docs/main/stable/typedef/ClientOptions"
  }
}

type MDNUrlKey = "ARRAY" | "STRING"

export const MDN_URLS: Record<MDNUrlKey, TypeURL> = {
  ARRAY: {
    type: "url",
    name: "Array",
    url:
      "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array"
  },
  STRING: {
    type: "url",
    name: "String",
    url:
      "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String"
  }
}
