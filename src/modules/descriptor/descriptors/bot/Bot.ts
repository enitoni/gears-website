import { createReference } from "../../helpers"
import {
  ClassDescriptor,
  InterfaceDescriptor,
  MethodDescriptor
} from "../../types"
import { CommandLike } from "../command/references"
import { DISCORDJS_URLS, MDN_URLS } from "../urls"

const CATEGORY = "Bot"

export const BotMetadata: InterfaceDescriptor = {
  name: "BotMetadata",
  kind: "interface",
  category: CATEGORY,
  properties: [
    {
      name: "name",
      kind: "interface-property",
      type: {
        ref: MDN_URLS.STRING
      }
    },
    {
      name: "description",
      kind: "interface-property",
      type: {
        ref: MDN_URLS.STRING
      },
      optional: true
    }
  ]
}

export const BotOptions: InterfaceDescriptor = {
  name: "BotOptions",
  kind: "interface",
  category: CATEGORY,
  description: "Options passed to the bot to configure it.",
  properties: [
    { name: "token", kind: "interface-property", type: { ref: "string" } },
    {
      name: "matcher",
      kind: "interface-property",
      type: {
        ref: "CommandMatcher"
      },
      optional: true
    },
    {
      name: "metadata",
      kind: "interface-property",
      type: {
        ref: createReference(BotMetadata)
      },
      optional: true
    },
    {
      name: "services",
      kind: "interface-property",
      type: {
        ref: MDN_URLS.ARRAY,
        params: [{ ref: "ServiceConstructor" }]
      },
      optional: true
    },
    {
      name: "commands",
      kind: "interface-property",
      type: {
        ref: MDN_URLS.ARRAY,
        params: [{ ref: CommandLike }]
      },
      optional: true
    }
  ]
}

const BotProcessMessage: MethodDescriptor = {
  name: "processMessage",
  description:
    "Process a discord message through the bot, this is called automatically when a message is received.",
  kind: "method",
  async: true,
  arguments: [
    {
      name: "message",
      type: {
        ref: DISCORDJS_URLS.MESSAGE
      }
    }
  ]
}

const BotStart: MethodDescriptor = {
  name: "start",
  description: "Start the bot",
  kind: "method",
  async: true
}

export const Bot: ClassDescriptor = {
  name: "Bot",
  description: "Represents a Gears bot",
  kind: "class",
  category: CATEGORY,
  constructorArgs: [
    {
      name: "options",
      type: {
        ref: createReference(BotOptions)
      }
    },
    {
      name: "clientOptions",
      type: {
        ref: DISCORDJS_URLS.CLIENT_OPTIONS
      },
      optional: true
    }
  ],
  methods: [BotStart, BotProcessMessage]
}
