import { DescriptorReference } from "../../types"

export const Bot: DescriptorReference<"class"> = {
  type: "reference",
  name: "Bot",
  kind: "class"
}
