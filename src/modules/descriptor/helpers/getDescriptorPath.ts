import { REFERENCE_PATH } from "../../docs/constants"
import { DescriptorLike } from "../types"

export const getDescriptorPath = (
  descriptor: DescriptorLike,
  context?: DescriptorLike
) => {
  return context
    ? `${REFERENCE_PATH}/${context.kind}/${context.name}#${descriptor.name}`
    : `${REFERENCE_PATH}/${descriptor.kind}/${descriptor.name}`
}
