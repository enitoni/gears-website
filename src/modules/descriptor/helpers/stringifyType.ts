import { Type } from "../types"

export const stringifyType = (type: Type) => {
  if (typeof type === "string") return type
  return type.name
}
