import { Descriptor, DescriptorReference } from "../types"
import { DescriptorLike } from "../types/DescriptorLike"

export const createReference = <T extends DescriptorLike["kind"]>(
  descriptor: Descriptor<T>,
  array = false
): DescriptorReference<T> => ({
  array,
  type: "reference",
  kind: descriptor.kind,
  name: descriptor.name
})
