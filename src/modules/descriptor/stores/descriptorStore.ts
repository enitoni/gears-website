import { bind } from "decko"
import * as descriptors from "../descriptors"
import { DescriptorLike } from "../types/DescriptorLike"

class DescriptorStore {
  public readonly descriptors: DescriptorLike[] = Object.values(descriptors)

  @bind
  public get(name: string, kind?: string) {
    return this.descriptors.find(
      descriptor =>
        descriptor.name === name && (!kind || descriptor.kind === kind)
    )
  }
}

export const descriptorStore = new DescriptorStore()
