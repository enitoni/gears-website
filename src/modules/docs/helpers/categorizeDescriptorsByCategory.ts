import { SidebarCategory } from "../../core/components"
import { DescriptorLike } from "../../descriptor/types"
import { getDescriptorSidebarItem } from "./getDescriptorSidebarItem"

export const categorizeDescriptorsByCategory = (
  descriptors: DescriptorLike[]
) => {
  const categories: SidebarCategory[] = []

  const getOrCreateCategory = (name: string) => {
    const category = categories.find(c => c.name === name)
    if (category) return category

    const newCategory: SidebarCategory = {
      name,
      items: []
    }

    categories.push(newCategory)
    return newCategory
  }

  for (const descriptor of descriptors) {
    const category = getOrCreateCategory(descriptor.category || "Uncategorized")
    category.items.push(getDescriptorSidebarItem(descriptor))
  }

  return categories.sort((a, b) => (a.name > b.name ? 1 : -1))
}
