import { SidebarItem } from "../../core/components"
import { getDescriptorPath } from "../../descriptor/helpers"
import { DescriptorLike } from "../../descriptor/types"

export const getDescriptorSidebarItem = (
  descriptor: DescriptorLike,
  context?: DescriptorLike
): SidebarItem => {
  const children: SidebarItem[] = []

  const to = getDescriptorPath(descriptor, context)
  const result = {
    label: descriptor.name,
    child: !!context,
    children,
    to
  }

  const get = (d: DescriptorLike) => getDescriptorSidebarItem(d, descriptor)

  if (descriptor.kind === "class") {
    const { properties = [], methods = [] } = descriptor
    children.push(...properties.map(get), ...methods.map(get))
  }

  return result
}
