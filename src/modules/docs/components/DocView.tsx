import React from "react"
import { Sidebar, SidebarCategory } from "../../core/components"
import { DescriptorView } from "../../descriptor/components"
import { descriptorStore } from "../../descriptor/stores"
import { Route, Router } from "../../routing/components"
import { categorizeDescriptorsByCategory } from "../helpers"
import { Document, DocumentTitle } from "./"
import "./DocView.scss"

const categories: SidebarCategory[] = [
  {
    name: "Guides",
    items: [
      {
        label: "Getting started",
        to: "/docs/guides/getting-started"
      }
    ]
  },
  ...categorizeDescriptorsByCategory(descriptorStore.descriptors)
]

const routes: Route[] = [
  {
    match: "/docs",
    component: () => <div>Hello!</div>
  },
  {
    match: "/docs/guides(/*)",
    component: () => (
      <Document>
        <DocumentTitle title="Guides" />
      </Document>
    )
  },
  {
    match: "/docs/api(/:kind/:name)",
    component: DescriptorView
  }
]

export const DocView = () => {
  return (
    <div className="DocView">
      <Sidebar categories={categories} />
      <div className="body">
        <Router routes={routes} />
      </div>
    </div>
  )
}
