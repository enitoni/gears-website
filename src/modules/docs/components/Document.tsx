import React from "react"
import "./Document.scss"

export const Document = (props: React.Props<{}>) => (
  <article className="Document">{props.children}</article>
)
