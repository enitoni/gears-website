import React from "react"
import "./DocumentTitle.scss"

export interface DocumentTitleProps {
  title: string
}

export const DocumentTitle = (props: DocumentTitleProps) => {
  return <h1 className="DocumentTitle">{props.title}</h1>
}
