import React from "react"
import { DocView } from "../../docs/components"
import { Route, Router } from "../../routing/components"
import "./Body.scss"

const routes: Route[] = [
  {
    match: "/",
    component: () => <div>Hello!</div>
  },
  {
    match: "/docs(/*)",
    component: DocView
  }
]

export const Body = () => (
  <main className="Body">
    <div className="content">
      <Router routes={routes} />
    </div>
  </main>
)
