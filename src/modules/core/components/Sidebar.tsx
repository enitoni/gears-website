import getClassName from "classnames"
import React from "react"
import { RouteLink, RouteLinkProps } from "../../routing/components"
import "./Sidebar.scss"

export interface SidebarItem extends RouteLinkProps {
  label: string
  children?: SidebarItem[]
  child?: boolean
}

export interface SidebarCategory {
  name: string
  items: SidebarItem[]
}

export interface SidebarProps {
  categories: SidebarCategory[]
}

export const SidebarItem = (props: SidebarItem) => {
  const { label, children = [], child, ...routeProps } = props

  const renderedChildren = children.map(SidebarItem)
  const className = getClassName("SidebarItem", {
    "-child": child
  })

  return (
    <li key={props.to} className={className}>
      <RouteLink className="link" {...routeProps}>
        {label}
      </RouteLink>
      <ul className="children">{renderedChildren}</ul>
    </li>
  )
}

export const Sidebar = (props: SidebarProps) => {
  const { categories } = props

  const renderedCategories = categories.map(category => {
    const renderedItems = category.items.map(SidebarItem)

    return (
      <div key={category.name} className="category">
        <h1 className="name">{category.name}</h1>
        <ul className="items">{renderedItems}</ul>
      </div>
    )
  })

  return <div className="Sidebar">{renderedCategories}</div>
}
