import React from "react"
import "./Header.scss"
import { HeaderLink } from "./HeaderLink"

export const Header = () => (
  <header className="Header">
    <div className="content">
      <h1 className="logo">Gears</h1>
      <nav className="nav">
        <HeaderLink to="/" label="Home" />
        <HeaderLink to="/docs" match="/docs(/*)" label="Docs" />
      </nav>
    </div>
  </header>
)
