import React from "react"
import { RouteLink, RouteLinkProps } from "../../routing/components"
import "./HeaderLink.scss"

export interface HeaderLinkProps extends RouteLinkProps {
  label: string
}

export const HeaderLink = (props: HeaderLinkProps) => {
  const { label } = props

  return (
    <RouteLink className="HeaderLink" {...props}>
      {label}
    </RouteLink>
  )
}
